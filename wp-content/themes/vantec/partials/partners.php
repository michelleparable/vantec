<?php
// Template Name: Partners
the_post();
get_header(); ?>

<section class="partners">
    <div class="partners__landing" style="background: url(<?php echo get_the_post_thumbnail_url(); ?>); background-position:center;background-size: cover;">
    </div>
    <div class="partners__story">
        <h1><?php the_field('title');?></h1>
        <p><?php the_field('intro');?></p>
    </div>
    <div class="partners__body">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-8">
                    <div class="row partners-logos">
                        <?php if(have_rows('partners')):
                    while(have_rows('partners')) : the_row(); ?>
                        <div class="col-xs-12 col-sm-4 col-md-3">
                            <div class="partner-logo">
                                <a href="<?php the_sub_field('link');?>" target="_blank"><img
                                        src="<?php the_sub_field('logo');?>"></a>
                            </div>
                        </div>
                        <?php endwhile;
                    endif;?>
                    </div>
                </div>
                <div class="col-md-12 col-lg-4">
                    <div class="interior__body--slider">
                        <div class="interior-slider">
                        <?php 
							$images = get_field('slider');
							if( $images ): ?>
                            <?php foreach( $images as $image ): ?>
                            <div class="interior-slider__image">
                                <img src="<?php echo $image;?>">
                            </div>
                            <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="interior__body--video">
                        <video controls>
                            <source src="<?php lp_image_dir(); ?>/vantec_comparison.mp4" type="video/mp4">
                            Sorry, your browser doesn't support embedded videos.
                        </video>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Registration section added -->
    <?php lp_theme_partial('/partials/enquiry.php'); ?>
    <!-- Registration section added End-->
</section>

<?php get_footer(); ?>