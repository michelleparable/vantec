<?php
// Template Name: Landing
the_post();
get_header(); ?>

<section class="landing" id="landing">

    <div class="wrapper">
         <div class="down-arrow">
            <a href="#landing-body">Click to go down<br/><i class="fas fa-chevron-circle-down"></i></a>
        </div>
        <div class="landing__wrap">
             <?php $images = get_field('slider');
            if( $images ): ?>
                <?php foreach( $images as $image ): ?>
                    <div class="landing__wrap--slider" style="background: url(<?php echo $image;?>) no-repeat center/cover;">
                        <div class="banner-img">
                            <h1><?php the_field('title');?></h1>
                            <!-- <img src="<?php lp_image_dir(); ?>/moto-white.png"/><br>-->
                             <p><?php the_field('intro');?></p>
                        </div>
                        <div class="overlay-wrap"></div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>

        </div>
    </div>

    <div class="landing-body" id="landing-body">
        <div class="container">
            <div class="landing-body__intro">
                <div class="row">
                    <div class="col-md-12 col-lg-4">
                        <div class="intro-wrapper">
                            <div class="intro-copy">
                                <h2><?php the_field('interior_title');?></h2>
                                <p><?php the_field('interior_content');?></p>
                            </div>
                            <div class="intro-link">
                                <a href="<?php echo get_site_url(); ?>/interior">Learn more</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-4">
                        <div class="intro-wrapper">
                            <div class="intro-copy">
                                <h2><?php the_field('interior_title');?></h2>
                                <p><?php the_field('interior_content');?></p>
                            </div>
                            <div class="intro-link">
                                <a href="<?php echo get_site_url(); ?>/interior">Learn more</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-4">
                        <div class="intro-wrapper">
                            <div class="intro-copy">
                                <h2><?php the_field('nanocyn_title');?></h2>
                                <p><?php the_field('nanocyn_content');?></p>
                            </div>
                            <div class="intro-link">
                                <a href="<?php echo get_site_url(); ?>/nanocyn">Learn more</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <div class="landing-video">
        <div class="container">
            <div class="landing-video__wrap">
                <div class="video-title">
                    <h3><?php the_field('video_title');?></h3>
                    <p><?php the_field('video_content');?></p>
                </div>
                <video controls>
                    <source src="<?php lp_image_dir(); ?>/vantec_comparison.mp4" type="video/mp4">
                    Sorry, your browser doesn't support embedded videos.
                </video>
            </div>
        </div>
    </div>
    <!-- Registration section added -->
    <?php lp_theme_partial('/partials/enquiry.php'); ?>
    <!-- Registration section added End-->
</section>

<!--<section class="landing__body" id="landing">
</section>-->




<?php get_footer(); ?>