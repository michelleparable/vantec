<?php
// Template Name: Testimonial
the_post();
get_header(); ?>

<section class="mediatest" id="mediatest">
    <div class="mediatest__landing">
		<!--<img src="<?php lp_image_dir(); ?>/bg.jpg">-->
	</div>
    <div class="mediatest__top">
   			<h1>Testimonial</h1>
    		<p>Vantec’s exclusive interior protection will help prevent staining caused from spillage. All fabrics and leather inside your RV are treated with Vantec’s Interior Protection including: curtains, pelmet boxes, upholstery, mattresses, bedheads, bedspreads, padding and all other soft furnishings.</p>
   </div>

        <div class="mediatest__body">
            <div class="container">
            	<div class="row">
            		<div class="col-md-12 col-lg-8">

            			<div class="mediatest__body--left">
            				<div class="row">
        						<div class="col-sm-12 col-md-6">
        							<div class="left-mediatest">
	            						<img src="<?php lp_image_dir(); ?>/bg.jpg">
	            					</div>
        						</div>
        						<div class="col-sm-12 col-md-6">
        							<div class="right-copy">
	            						<h3>Title</h3>
	            						<p>Vantec’s revolutionary paint preservation treatment locks in the original colour preventing chalking and makes light Vantec’s </p>
	            						<a class="video-btn" href="#" data-toggle="modal" data-src="https://www.youtube.com/embed/IP7uGKgJL8U" data-target="#myModal">Click for more</a>


	            					</div>
        						</div>
            				</div>
            			</div>
                        <div class="mediatest__body--left">
                            <div class="row">
                                <div class="col-sm-12 col-md-6">
                                    <div class="left-mediatest">
                                        <img src="<?php lp_image_dir(); ?>/bg.jpg">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6">
                                    <div class="right-copy">
                                        <h3>Title</h3>
                                        <p>Vantec’s revolutionary paint preservation treatment locks in the original colour preventing chalking and makes light Vantec’s </p>
                                        <a class="video-btn" href="#" data-toggle="modal" data-src="https://www.youtube.com/embed/IP7uGKgJL8U" data-target="#myModal">Click for more</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mediatest__body--left">
                            <div class="row">
                                <div class="col-sm-12 col-md-6">
                                    <div class="left-mediatest">
                                        <img src="<?php lp_image_dir(); ?>/bg.jpg">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6">
                                    <div class="right-copy">
                                        <h3>Title</h3>
                                        <p>Vantec’s revolutionary paint preservation treatment locks in the original colour preventing chalking and makes light Vantec’s </p>
                                        <a class="video-btn" href="#" data-toggle="modal" data-src="https://www.youtube.com/embed/IP7uGKgJL8U" data-target="#myModal">Click for more</a>
                                    </div>
                                </div>
                            </div>
                        </div>

           			</div>
           			<div class="col-md-12 col-lg-4">

           				<div class="mediatest__body--right">
           					<div class="mediatest-video">
           						<video controls>
	      						    <source src="<?php lp_image_dir(); ?>/vantec_comparison.mp4" type="video/mp4">
	      						    Sorry, your browser doesn't support embedded videos.
	      						</video>
           					</div>
           					<div class="mediatest-facebook">
           						<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FVantec-RV-Care-903533839801375%2F&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="340" height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-mediatest"></iframe>
           					</div>
           				</div>
           			</div>
            	</div>
            </div>
        </div>
    </div>
    <!-- Registration section added -->
	<?php lp_theme_partial('/partials/enquiry.php'); ?>
	<!-- Registration section added End-->
</section>





 

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-body">
            
              <iframe class="embed-responsive-item" src="" id="video"  allowscriptaccess="always" allow="autoplay"></iframe>
           
          </div>
        </div>
      </div>
    </div>


<?php get_footer(); ?>