<?php
// Template Name: single media
the_post();
get_header(); ?>

<section class="mediatest" id="mediatest">
    <div class="mediatest__landing">
		<!--<img src="<?php lp_image_dir(); ?>/bg.jpg">-->
	</div>

        <div class="mediatest__media_body">
            <div class="container">
                <div class="main-title">
                    <h2>Protecting Allan's RVs for 20+ Years</h2>
                </div>
            	<div class="row">
            		<div class="col-md-12 col-lg-6">
                        <div class="video-wrap">
                            <iframe src="https://www.youtube.com/embed/bS1MS1L5qyA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>

           			</div>
                    <div class="col-md-12 col-lg-6">
                        <div class="content-wrap">
                            <p>Vantec’s revolutionary paint preservation treatment locks in the original colour preventing chalking and makes light Vantec’s </p>
                        </div>
                    </div>
            	</div>
            </div>
        </div>
    <!-- Registration section added -->
	<?php lp_theme_partial('/partials/enquiry.php'); ?>
	<!-- Registration section added End-->
</section>

<?php get_footer(); ?>