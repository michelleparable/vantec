<?php
// Template Name: Contact
the_post();
get_header(); ?>

<section class="contact" id="contact">
    <div class="contact__landing" style="background: url(<?php echo get_the_post_thumbnail_url(); ?>); background-position:center;background-size: cover;">
    </div>

    <div class="contact__body">
        <div class="container">
             <div class="contact__body--title">
                <h1><?php the_field('title');?></h1>
            </div>
            <div class="row">
                <div class="col-md-12 col-lg-8">
                    <div class="contact__body--form">
                        <?php echo do_shortcode ('[ninja_form id=4]') ?>
                    </div>
                </div>
                <div class="col-md-12 col-lg-4">
                    <div class="contact__body--info">
                        <div class="card">
                            <div class="card-body">
                                <div class="title">
                                    <h3>Head Office</h3>
                                </div>
                                <div class="address_wrapper">
                                    <p><a href="https://goo.gl/maps/NZRqhef74n771hTVA" target="_blank"> <?php the_field('street');?><?php the_field('suburb');?> <?php the_field('state');?> <?php the_field('postcode');?></a></p>
                                    <p>
                                        <span>Phone:</span><a href="tel:<?php the_field('phone');?>"> <?php the_field('phone');?></a> <br>
                                        <span>Fax:</span> <a href="tel:<?php the_field('fax');?>"><?php the_field('fax');?></a> <br>
                                        <span>Email:</span> <a href="mailto:<?php the_field('email');?>"><?php the_field('email');?></a> <br>
                                    </p>
                                    <p>We are open<br><?php the_field('opening_hours');?><br> Closed public holidays</p>
                                </div>
                            </div>
                            <div class="facebook card-footer">
                                <a target="_blank" href="<?php the_field('facebook')?>">
                                    <i class="fa fa-facebook fa-lg"></i>
                                    Follow us on Facebook
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

<?php get_footer(); ?>