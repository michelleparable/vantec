<?php
// Template Name: About
the_post();
get_header(); ?>

<section class="about" id="about">
	<div class="about__landing" style="background: url(<?php echo get_the_post_thumbnail_url(); ?>); background-position:center;background-size: cover;">
	</div>



   <div class="about__story">
   			<h1><?php the_field('title');?></h1>
    		<p><?php the_field('content');?></p>
   </div>

    <div class="about__slider">
	<?php
	if( have_rows('about_slider') ):
    while( have_rows('about_slider') ) : the_row();?>
		<div class="about__slider--wrap" style="background: url(<?php The_sub_field('image'); ?>) no-repeat center/cover;">
			<div class="overlay"></div>

			<div class="about-copy">
				<h1><?php the_sub_field('title')?></h1>
	    		<p><?php the_sub_field('content');?></p>
			</div>
		</div>

    <?php endwhile;
	endif;?>

		
    </div>
    <!-- Registration section added -->
	<?php lp_theme_partial('/partials/enquiry.php'); ?>
	<!-- Registration section added End-->
</section>

<?php get_footer(); ?>