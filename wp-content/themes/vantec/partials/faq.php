<?php
// Template Name: FAQ
the_post();
get_header(); ?>

<section class="faq">
	<div class="faq__landing" style="background: url(<?php echo get_the_post_thumbnail_url(); ?>); background-position:center;background-size: cover;">
	</div>
	<div class="faq__top">
        <h1>Frequently Asked Questions</h1>
        <p>Following are main questions people ask in related to our products. please feel free to contact us for further details.</p>
    </div>


   <div class="faq__body">
   	 	<div class="container">
   	 		<h2>How is Vantec NextGen different from other RV paint protection products?</h2>
    		<p>Vantec NextGen is a new generation ceramic surface coating. Unlike traditional paint protection products which can break down over time, the combination of traditional and advanced ceramics in this formula delivering a coating that outlasts and out performs many other ceramic paint coatings. Vantec NextGen coating contains both Silicon Dioxide and Silicon Carbide. Silicon Dioxide creates gloss, shine and protects against environmental damage and UV, and Silicon Carbide creates hardness and durability. Vantec NextGen is backed by a nationwide lifetime warranty which is transferable.</p>

    		<h2>What should I expect when I take delivery of my RV – will there be any difference in the appearance of the exterior surfaces?</h2>
			<p>Essentially, ceramic coatings will enhance the colour and shine of your cladding. Visually, results will vary and this is dependent on the colour and type of cladding. The coating takes around 3 days to fully cure. Once curing is completed, the coating will achieve maximum gloss and protection.</p>

			<h2>How should I take care of my treated exterior surfaces?</h2>
			<p>Maintenance is quick and easy. Simply hand wash the RV using the Vantec NextGen carwash shampoo (or any pH neutral car wash). We recommend always washing the RV in the shade when the cladding is cool. Avoid using commercial car washes, which often dispense strong detergents which may contain solvents.</p>

			<h2>What do you mean when you say your exterior coatings are "Hydrophobic"</h2>
			<p>Hydrophobic means that the surface rejects water, this means that water sheets off the protected surface taking with it dirt and other contaminants. This leaves your RV cleaner for longer.</p>

			<h2>Should I use any waxes or polishes on my RV when it’s been coated with Vantec NextGen exterior coating?</h2>
			<p>Some polishes can be quite abrasive and cause swirl/scratch marks. Waxes break down and require reapplication. The good news is that our Vantec NextGen Exterior Coating eliminates the need to wax or polish your RV – saving time, money and energy!</p>

			<h2>I was told your coating was made using "nanotechnology" – What is this and what are the benefits for me?</h2>
			<p>Nanotechnology, put simply, is the "science of small". A nano sized particle = 1b metre! We engineer our coatings using nanotechnology. What this means for you is a smoother surface and superior coverage.</p>

			<h2>Are the exterior and interior coatings environmentally friendly?</h2>
			<p>Yes, all Vantec NextGen coatings are water-based. No harmful solvents are used in the formulas.</p>

			<h2>What is the difference between Vantec NextGen interior coating and a retail brand such as Scotchgard?</h2>
			<p>Scotchgard like many "off-the-shelf products" is a surface spray – when sprayed onto the surface it creates a barrier to spills. However, because it only seals the surface it does wear off over time and will need reapplication in the event you have a spill and clean the surface. Vantec NextGen seals the entire fibre and creates a permanent solution to staining and also protects against fading & discolouration. What is also unique about Vantec NextGen is Bactishield which is a powerful antibacterial system which prevents mould and mildew on the surface.</p>

			<h2>What are the main ingredients of the Vantec NextGen exterior surface coating and why is Vantec NextGen different to traditional exterior protection products?</h2>

			<p>The coating contains both silica (which is the main ingredient in glass and crystalware which produces a long lasting durable gloss ) and ceramic (which gives the coating incredible resistance to abrasion). Abrasion from road grime and dirt causes many traditional paint protection products break down over time. Vantec NextGen is a once-off permanent solution.</p>

			<h2>I have read that your interior coating contains Bactishield – what is this?</h2>
			<p>Bactishield is an anti-bacterial system that works to inhibit bacteria in the coated fibres. Bacteria can contribute to odours, and also potentially impact on health and wellbeing of some occupants of the vehicle. Bactishield also prevents mould and mildew which are often triggers for respiratory illnesses such as asthma.</p>

			<h2>Are there any solvents in your interior coating formula? Having allergies and being concerned about the environment I want to ensure your coating isn’t harmful?</h2>
			<p>No – our formula is water-based, making it kinder to not only the occupants of the RV, but to our environment!</p>

   	 	</div>
   </div>



    <!-- Registration section added -->
	<?php lp_theme_partial('/partials/enquiry.php'); ?>
	<!-- Registration section added End-->
</section>

<?php get_footer(); ?>