<?php
// Template Name: Nanocyn
the_post();
get_header(); ?>

<section class="interior" id="interior">
    <div class="interior__landing" style="background: url(<?php echo get_the_post_thumbnail_url(); ?>); background-position:center;background-size: cover;">
    </div>
    

    <div class="interior__title">
        <h1><?php the_field('title');?></h1>
        <p><?php the_field('intro');?></p>
    </div>
    <div class="interior__body">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class="interior__body--copy">
                        <div class="intro">
                            <h3>About Nanocyn</h3>
                        <?php the_field('content');?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <div class="edm-img">
                        <a href="https://www.purazine.com/authorised-industry-dealers" target="_blank"><img src="<?php lp_image_dir(); ?>/nanocyn_edm_01.jpg"></a>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                     <div class="edm-img">
                        <a href="https://www.purazine.com/authorised-industry-dealers" target="_blank"><img src="<?php lp_image_dir(); ?>/nanocyn_edm_02.jpg"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Registration section added -->
    <?php lp_theme_partial('/partials/enquiry.php'); ?>
    <!-- Registration section added End-->
</section>

<?php get_footer(); ?>


