<?php
// Template Name: Register
the_post();
get_header(); ?>

<section class="enquiry" id="enquiry">
    <div class="container">
    	<div class="enquiry__title">
    		<h1>Submit Your Enquiry</h1>
    	</div>
        <?php echo do_shortcode ('[ninja_form id=3]') ?>
    </div>
</section>

<?php get_footer(); ?>