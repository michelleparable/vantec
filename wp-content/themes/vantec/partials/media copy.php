<?php
// Template Name: media
the_post();
get_header(); ?>

<section class="mediatest" id="mediatest">
    <div class="mediatest__landing">
        <!--<img src="<?php lp_image_dir(); ?>/bg.jpg">-->
    </div>
    <div class="mediatest__top">
        <h1><?php the_field('title');?></h1>
        <p><?php the_field('intro');?></p>
    </div>

    <div class="mediatest__body">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-8">
                <?php if(have_rows('media')):
                    while(have_rows('media')) : the_row(); ?>

                    <div class="mediatest__body--left">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <div class="left-mediatest">
                                    <img src="<?php the_sub_field('image');?>">
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <div class="right-copy">
                                    <h3><?php the_sub_field('title');?></h3>
                                    <p><?php echo wp_trim_words( get_sub_field('content'), 15, '...'); ?></p>
                                    <a href="#" data-toggle="modal" data-target="#videoModal<?php the_sub_field('unique_id'); ?>">Click for more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endwhile;
                                endif;?>

                </div>
                <div class="col-md-12 col-lg-4">

                    <div class="mediatest__body--right">
                        <div class="mediatest-video">
                            <video controls>
                                <source src="<?php lp_image_dir(); ?>/vantec_comparison.mp4" type="video/mp4">
                                Sorry, your browser doesn't support embedded videos.
                            </video>
                        </div>
                        <div class="mediatest-facebook">
                            <iframe
                                src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FVantec-RV-Care-903533839801375%2F&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId"
                                width="340" height="500" style="border:none;overflow:hidden" scrolling="no"
                                frameborder="0" allowTransparency="true" allow="encrypted-mediatest"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- Modal -->
    <?php if(have_rows('media')):
    while(have_rows('media')) : the_row(); ?>

    <div class="modal fade iframeStop" id="videoModal<?php the_sub_field('unique_id'); ?>">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                    <div class="modal-body">
                        <div class="mediatest__media_body">
                            <div class="container">
                                <div class="main-title">
                                    <h2><?php the_sub_field('title');?></h2>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-lg-6">
                                        <div class="video-wrap">
                                            <iframe id="iframeModal" src="<?php the_sub_field('video');?>" frameborder="0"
                                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                allowfullscreen></iframe>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-6">
                                        <div class="content-wrap">
                                            <p><?php the_sub_field('content');?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    
    <?php endwhile;
    endif;?>
    <!-- End of Modal -->
    <!-- Registration section added -->
    <?php lp_theme_partial('/partials/enquiry.php'); ?>
    <!-- Registration section added End-->
</section>

<?php get_footer(); ?>