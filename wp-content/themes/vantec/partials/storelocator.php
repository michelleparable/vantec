<?php
// Template Name: Store Locator
the_post();
get_header(); ?>

<section class="storelocator">
    <div class="storelocator__landing" style="background-image: url('<?php lp_image_dir(); ?>/bg.jpg'); background-position:center;background-size: cover;">
        <!--<img src="<?php lp_image_dir(); ?>/bg.jpg">-->
    </div>
    <div class="container">
        <div class="storelocator__title">
            <h1>Find Vantec Provider</h1>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
        </div>
    </div>
     <div class="storelocator__locator">
        <div class="container">
            <?php echo do_shortcode ('[wpsl]') ?>
        </div>
    </div>



    <!-- Registration section added -->
	<?php lp_theme_partial('/partials/enquiry.php'); ?>
	<!-- Registration section added End-->
</section>

<?php get_footer(); ?>