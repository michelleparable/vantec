<?php
// Template Name: Interior
the_post();
get_header(); ?>

<section class="interior" id="interior">
    <div class="interior__landing" style="background: url(<?php echo get_the_post_thumbnail_url(); ?>); background-position:center;background-size: cover;">
    </div>
    

    <div class="interior__title">
        <h1><?php the_field('title');?></h1>
        <p><?php the_field('intro');?></p>
    </div>
    <div class="interior__body">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-6">
                    <div class="interior__body--copy">
                        <div class="intro">
                            <h3>Benetfits</h3>
                        <?php the_field('content');?>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-6">
                    <div class="interior__body--slider">
                        <div class="interior-slider">
                            <?php 
							$images = get_field('slider');
							if( $images ): ?>
                            <?php foreach( $images as $image ): ?>
                            <div class="interior-slider__image">
                                <img src="<?php echo $image;?>">
                            </div>
                            <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="interior__body--video">
                        <video controls>
                            <source src="<?php lp_image_dir(); ?>/vantec_comparison.mp4" type="video/mp4">
                            Sorry, your browser doesn't support embedded videos.
                        </video>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Registration section added -->
    <?php lp_theme_partial('/partials/enquiry.php'); ?>
    <!-- Registration section added End-->
</section>

<?php get_footer(); ?>