<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>

	<script type="text/javascript">
		var _ajaxurl = '<?= admin_url("admin-ajax.php"); ?>';
		var _pageid = '<?= get_the_ID(); ?>';
		var _imagedir = '<?php lp_image_dir(); ?>';
	</script>

	<link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet"><!--  no italic font added -->

	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php lp_image_dir(); ?>/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php lp_image_dir(); ?>/favicon/favicon-16x16.png">
	<link rel="manifest" href="/site.webmanifest">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">

</head>
<body <?php body_class(); ?>>

<?php

// This fixes an issue where wp_nav_menu applied the_title filter which causes WC and plugins to change nav menu labels
print '<!--';
the_title();
print '-->';

?>

<div id="top"></div>
<div class="wrapper">
	<header class="site-header">
		<div class="container">
			<div class="menu-wrapper">
				<div class="menu-wrapper__top">
					<div class="menu-wrapper__top--logo">
						<a href="<?php echo get_home_url(); ?>"><img class="logo" src="<?php lp_image_dir(); ?>/vantec-logo-white.png"></a>
					</div>
					<div class="menu-wrapper__top--contact">
						<h5>Call us on <br>
						<a href="tel:0269211198">(02) 6921 1198</a></h5>
					</div>
				</div>
				<div class="menu-wrapper__bottom" id="main-menu">
					<div class="menu-wrapper__bottom--menu">
						<?php
						wp_nav_menu( array(
						    'theme_location' => 'header-menu',
						    'container_class' => 'custom-menu-class' ) );
						?>
					</div>
				</div>
			</div>
		</div>
	</header>


