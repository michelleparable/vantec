	<footer class="site-footer">
		<!-- Footer goes here -->
		 <div class="site-footer__wrapper">
		 	<div class="container">
                <div class="row">
                	<div class="col-xs-12 col-sm-4 col-md-3">
                        <div class="logo">
                            <a href="<?php echo get_site_url(); ?>" target="_blank"><img src="<?php lp_image_dir(); ?>/vantec-logo-white.png"></a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-3">
                    	 <ul>
                            <li><a href="<?php echo get_site_url(); ?>/about/" target="_blank">About Us</a></li>
                            <!--<li><a href="<?php echo get_site_url(); ?>/testimonials/" target="_blank">Testimonials</a></li>-->
                            <li><a href="<?php echo get_site_url(); ?>/partners/" target="_blank">Partners</a></li>
                            <li><a href="<?php echo get_site_url(); ?>/contact/" target="_blank">Contact Us</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-3">
                    	 <ul>
                            <li><a href="<?php echo get_site_url(); ?>/interior/">Interior Protection</a></li>
                            <li><a href="<?php echo get_site_url(); ?>/exterior/">Exterior Protection</a></li>
                            <!-- <li><a href="<?php echo get_site_url(); ?>/accesorries/">Accesorries</a></li> -->
                            <li><a href="<?php echo get_site_url(); ?>/media/">Media</a></li>
                            <li><!-- https://www.positivessl.com/the-positivessl-trustlogo - positive SSL secure certificate -->
                            <script type="text/javascript"> //<![CDATA[
                              var tlJsHost = ((window.location.protocol == "https:") ? "https://secure.trust-provider.com/" : "http://www.trustlogo.com/");
                              document.write(unescape("%3Cscript src='" + tlJsHost + "trustlogo/javascript/trustlogo.js' type='text/javascript'%3E%3C/script%3E"));
                            //]]></script>
                            <script language="JavaScript" type="text/javascript">
                              TrustLogo("https://www.positivessl.com/images/seals/positivessl_trust_seal_lg_222x54.png", "POSDV", "none");
                            </script></li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-3">
                        <div class="addres-info">
                        	<a target="_blank" href="https://goo.gl/maps/4schHQLDXJr78GNf9"><?php the_field('street', 'option');?> <br/> <?php the_field('suburb', 'option');?> <?php the_field('state', 'option');?> <?php the_field('postcode', 'option');?></a>
                        </div>
                        <div class="addres-phone">
                            <a href="tel:<?php the_field('phone', 'option');?>"><?php the_field('phone', 'option');?></a>
                        </div>
                        <div class="addres-email">
                            <a href="mailto:<?php the_field('email', 'option');?>"><?php the_field('email', 'option');?></a>
                        </div>
                        <div class="addres-social">
                            <a class="youtube" target="_blank" href="https://www.youtube.com/channel/UC2ExqZLFKQLkoWYmbCyTCwQ"><i class="fab fa-youtube"></i></a>
                             <a class="facebook" target="_blank" href="https://www.facebook.com/Vantec-RV-Care-903533839801375/"><i class="fab fa-facebook-square"></i></a>
                            
                        </div>
                    </div>
                </div>
            </div>
		 </div>
	</footer>
</div> <!-- end wrapper -->
<?php wp_footer(); ?>



</body>
</html>